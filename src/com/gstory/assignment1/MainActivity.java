package com.gstory.assignment1;

import android.R.color;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * This program will ask the user to input there name and number. based on the
 * name the user will be given a story about Fuzzy Wuzzy the Bear.
 * 
 * @author Gyasi Story
 * 
 */

public class MainActivity extends Activity {

	// Layout Variable
	LinearLayout layout;
	LinearLayout.LayoutParams Params;
	EditText name;
	String gotName;
	TextView finish;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// the Layout
		layout = new LinearLayout(this);
		layout.setOrientation(LinearLayout.VERTICAL);
		layout.setPadding(10, 10, 10, 10);
		Params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		layout.setLayoutParams(Params);

		// Setting Header Text
		TextView header = new TextView(this);
		header.setTextSize(20);
		header.setTextColor(Color.BLUE);
		header.setBackgroundColor(Color.LTGRAY);
		header.setPadding(0, 0, 0, 30);
		header.setText("This is the Story of Fuzzy Wuzzy");
		layout.addView(header);

		// Load Story Story sentences
		String[] sentence = { getResources().getString(R.string.Bear),
				getResources().getString(R.string.Hair),
				getResources().getString(R.string.Scare),
				getResources().getString(R.string.There) };
		
		//New Text View
		TextView senText = new TextView(this);
		String stringText = "";

		for ( int i = 0; i < sentence.length; i++) {
			//Start Story with out Name
			stringText += sentence[i] + "\r\n";			

		}
		senText.setText(stringText);
		layout.addView(senText);
		
		
		//Adding Edit Text
		name = new EditText(this);
		name.setHint("enter your name");
		layout.addView(name);
		
		//Finish Story
		finish = new TextView(this);
		finish.setTextColor(color.holo_blue_light);
		
		//add Button
		Button start = new Button(this);
		start.setText("Start");
		start.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// get Name
				gotName = name.getText().toString();
				Log.d("Is This it", gotName);
				
				
				
				if (gotName.length()<2){
					finish.setText("Enter your Name to finish story");
				} else {
					finish.setText(gotName + " is scared of Fuzzy \r\n" +
							gotName + " will run from Fuzzy \r\n" +
							gotName + " won't tell any one about Fuzzy\r\n");
				}
				
				
			}
		});
		layout.addView(start);
		layout.addView(finish);
		
		
		
		
		
		setContentView(layout);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
}
